﻿$("#submitid").click(function () {
    var id = document.getElementById("sessionid").value;
    $("#container").hide();
    $.ajax({
        url: '/Session/GetSessionDitails',
        type: 'POST',
        data: {'id':id},
        //contentType: 'application/json; charset=utf-8',
        success: function (data) {
                var apiKey = "44702572";
                var sessionId = data.id;
                var token = data.token;
                var publisher = TB.initPublisher(apiKey);
                var session = TB.initSession(sessionId);

                session.connect(apiKey, token);
                session.addEventListener("sessionConnected",
                                         sessionConnectedHandler);

                session.addEventListener("streamCreated",
                                         streamCreatedHandler);
            
        },
        error: function () {
            $("#container").show();
            alert("Nie istnieje sesja o podanym id");
        }
    });
});