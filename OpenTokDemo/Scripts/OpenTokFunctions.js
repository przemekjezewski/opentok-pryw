﻿
function sessionConnectedHandler(event) {
    session.publish(publisher);
    subscribeToStreams(event.streams);
}
function subscribeToStreams(streams) {
    for (var i = 0; i < streams.length; i++) {
        var stream = streams[i];
        if (stream.connection.connectionId
               != session.connection.connectionId) {
            session.subscribe(stream);
        }
    }
}
function streamCreatedHandler(event) {
    subscribeToStreams(event.streams);
}