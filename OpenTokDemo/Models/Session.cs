﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OpenTokDemo.Models
{
    public class Session
    {
        public string id { get; set; }
        public string token { get; set; }
        [DataType(DataType.DateTime)]
        [Required]
        public DateTime timeExpired { get; set; }
    }
    public class SessionContext:DbContext
    {
        public DbSet<Session> Sessions { get; set; }
    }
}