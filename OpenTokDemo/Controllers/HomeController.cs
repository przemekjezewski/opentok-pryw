﻿using OpenTokDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OpenTokDemo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Index(Session session)
        {
            using(var context=new SessionContext())
            {
                session=context.Sessions.Find(session.id);
            }
            if (session != null)
                return View("StartTok", session);
            return View();
        }
        

        

        
    }
}