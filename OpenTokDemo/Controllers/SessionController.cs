﻿using OpenTok;
using OpenTokDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace OpenTokDemo.Controllers
{
    public class SessionController : Controller
    {
        public ActionResult Index()
        {
            using (var context = new SessionContext())
            {

                var model = context.Sessions.ToList();
                return View(model);
            }
        }
        public ActionResult Delete(string id)
        {
            using (var context = new SessionContext())
            {

                var session = context.Sessions.First(s=>s.id==id);
                
                return View(session);
            }
           
        }
        [HttpPost]
        public ActionResult Delete(Session trash)
        {
            using (var context = new SessionContext())
            {
                trash = context.Sessions.Find(trash.id);
                context.Sessions.Remove(trash);
                context.SaveChanges();
                var model = context.Sessions.ToList();
                return View("Index", model);
            }
            

        }
        
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add(Session newSession)
        {
            if (ModelState.IsValid)
            {
                OpenTokSDK opentok = new OpenTokSDK();
                Dictionary<string, object> options = new Dictionary<string, object>();
                options.Add(SessionPropertyConstants.P2P_PREFERENCE, "enabled");
                string sessionId = opentok.CreateSession(Request.ServerVariables["REMOTE_ADDR"], options);
                Dictionary<string, object> tokenOptions = new Dictionary<string, object>();
                tokenOptions.Add(TokenPropertyConstants.EXPIRE_TIME, newSession.timeExpired);
                tokenOptions.Add(TokenPropertyConstants.ROLE, RoleConstants.SUBSCRIBER);
                string token = opentok.GenerateToken(sessionId,tokenOptions);
                
                using (var context = new SessionContext())
                {
                    newSession.id = sessionId;
                    newSession.token = token;
                    context.Sessions.Add(newSession);
                    context.SaveChanges();
                    var model = context.Sessions.ToList();
                    return View("Index", model);

                }
            }
            else
                return View();
        }
        [HttpPost]
        public JsonResult GetSessionDitails(string id)
        {
            Session session;
            using (var context = new SessionContext())
            {
                session = context.Sessions.Find(id);   
            }
            return Json(session);
        }
	}
}